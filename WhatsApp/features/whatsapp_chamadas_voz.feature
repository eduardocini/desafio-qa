#language: pt
Funcionalidade: Chamadas de voz via WhatsApp

Cenário: Iniciar uma nova chamada de audio
Dado Que eu esteja na tela CHAMADAS
E e toque no ícone de um telefone com sinal de + ao lado
E e visualize o contato para realizar a chamada
Quando toco ícone de telefone
Então inicia uma chamada por voz


Cenário: Excluir historico de chamadas de voz
Dado Dado Que eu esteja na tela CHAMADAS
E e selecione a opções
E e selecione a opção Limpar chamadas
Quando toco na opção OK
Então todo historico de chamadas será excluido