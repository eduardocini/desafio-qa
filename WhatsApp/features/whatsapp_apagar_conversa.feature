#language: pt
Funcionalidade: Apagar conversa privada

Cenário: Apagar todo historico de conversa privada
Dado Que eu esteja na conversa privada
E e selecione a opções
E e selecione a opção Mais
E e selecione a opção Limpar conversa
Quando seleciono na opção Limpar
Então o conteudo da conversa deve ser apagado

Cenário: Cancelar Limpar conversa
Dado Que eu esteja na conversa privada
E e abra o menu opções
E e selecione a opções
E e selecione a opção Mais
E e selecione a opção Limpar conversa
Quando seleciono na opção CANCELAR
Então o conteudo da conversa continue visível

Cenário: Apagar uma unica mensagem na conversa privada
Dado Que eu esteja na conversa privada
E e toque na mensagem especifica mantendo pressionado até toda a linha ficar em destaque
E e toque no ícone lixeira
Quando seleciono na opção APAGAR
Então a mensagem especifica será apagada