#!/usr/bin/env python
# coding:utf-8
# Author: Eduardo Cini Simoni
# Purpose: http://codekata.com/kata/kata09-back-to-the-checkout/
# Created: 22/02/2017

import fnmatch
import unittest
import collections


class Checkout:
    def __init__(self, ):
        self.items = ''
        self.rules = {'A': {1: 50, 3: 130},
                      'B': {1: 30, 2: 45},
                      'C': {1: 20},
                      'D': {1: 15}}

    # ----------------------------------------------------------------------
    def new_rule(self, item, price, quantity=1):
        if len(item) > 1:
            # Obrigatorio o item ser apenas um caracter.
            raise ValueError('Item name must have only one character')

        if item not in self.rules:
            if quantity > 1:
                # So sera possivel criar promocao se ja existir valor unitario do item
                raise ValueError('You must have insert first unit price')
            self.rules[item] = {}

        self.rules[item][quantity] = price

    # ----------------------------------------------------------------------
    def price(self):
        total_price = 0
        self.items = ''.join(sorted(self.items))

        self.rules = collections.OrderedDict(sorted(self.rules.items()))
        for _item, prices in self.rules.items():
            prices = collections.OrderedDict(reversed(sorted(prices.items())))
            for qnt, item_price in prices.items():
                total_qnt = _item * qnt
                if total_qnt in self.items:
                    total_price += item_price * self.items.count(total_qnt)
                    self.items = self.items.replace(total_qnt, '')

        # print(total_price)
        return total_price

    # ----------------------------------------------------------------------
    def scan(self, item):
        self.items += item


# ----------------------------------------------------------------------
if __name__ == '__main__':
    pass
    # a = Checkout()
    # a.new_rule('E', 25)
    # a.scan('ABBAADDABAAAAAE')
    # a.price()
