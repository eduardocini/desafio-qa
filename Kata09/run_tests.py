__author__ = 'Eduardo'

import unittest
from price_calculator import Checkout


class TestPrice(unittest.TestCase):
    def setUp(self):
        self.check = Checkout()
        self.rules = self.check.rules

    def test_without_any_item(self):
        self.check.scan('')
        self.assertEqual(0, self.check.price())

    def test_with_single_type_item(self):
        self.check.scan('A')
        self.assertEqual(50, self.check.price())

        self.check.scan('AB')
        self.assertEqual(80, self.check.price())

        self.check.scan('CDBA')
        self.assertEqual(115, self.check.price())

    def test_with_equals_items(self):
        self.check.scan('AA')
        self.assertEqual(100, self.check.price())

        self.check.scan('AAA')
        self.assertEqual(130, self.check.price())

        self.check.scan('AAAA')
        self.assertEqual(180, self.check.price())

        self.check.scan('AAAAA')
        self.assertEqual(230, self.check.price())

        self.check.scan('AAAAAA')
        self.assertEqual(260, self.check.price())

    def test_with_multiples_differents_items(self):
        self.check.scan('AAAB')
        self.assertEqual(160, self.check.price())

        self.check.scan('AAABB')
        self.assertEqual(175, self.check.price())

        self.check.scan('AAABBD')
        self.assertEqual(190, self.check.price())

        self.check.scan('DABABA')
        self.assertEqual(190, self.check.price())

        self.check.scan('ABBAADDABAAAAA')
        self.assertEqual(495, self.check.price())

    def test_incremental(self):
        self.check.scan('A')
        self.check.scan('B')
        self.check.scan('A')
        self.check.scan('A')
        self.check.scan('B')
        self.assertEqual(175, self.check.price())

    def test_with_a_new_rule(self):
        self.check.scan('AAABB')
        self.check.new_rule('E', 25)
        self.check.scan('E')
        self.assertEqual(200, self.check.price())
        

if __name__ == '__main__':
    unittest.main()

